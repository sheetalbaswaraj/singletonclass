package com.sheetal;

public class SingetonClass2 {
	static  SingetonClass2  s1= null ;

	 private SingetonClass2()
	 {
		 System.out.println("invoked only once");
	 }
	 static SingetonClass2 getSingletonObject()
	 {
		 if(s1==null) {
			 s1=new SingetonClass2() ;
		 }
		 return s1;
	 }
}
class SingletonMain1{
	public static void main(String[] args) {
		
		SingetonClass2 s1=	SingetonClass2.getSingletonObject();
		SingetonClass2 s2=	SingetonClass2.getSingletonObject();
		 
	}
}


