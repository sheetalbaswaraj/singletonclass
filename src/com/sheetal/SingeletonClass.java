package com.sheetal;

public class SingeletonClass {
	
	static  SingeletonClass  s1= new SingeletonClass() ;

	 private SingeletonClass()
	 {
		 System.out.println("invoked only once");
	 }
	 static SingeletonClass getSingletonObject()
	 {
		 return s1;
	 }
}
class SingletonMain{
	public static void main(String[] args) {
		
		SingeletonClass s1=	SingeletonClass.getSingletonObject();
		SingeletonClass s2=	SingeletonClass.getSingletonObject();
		 
	}
}